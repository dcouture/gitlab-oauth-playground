# frozen_string_literal: true

require 'sinatra'
require 'securerandom'
require 'digest'
require 'base64'
require 'httparty'

USERNAME = ENV['OAUTH_USERNAME']
PASSWORD = ENV['OAUTH_PASSWORD']
GITLAB_URL = 'http://gitlab.com'
CLIENT_ID = ENV['OAUTH_CLIENT_ID']
CLIENT_SECRET = ENV['OAUTH_CLIENT_SECRET']
CODE_VERIFIER = SecureRandom.hex(30)
CODE_CHALLENGE = Base64.urlsafe_encode64(Digest::SHA256.digest(CODE_VERIFIER), padding: false)
STATE = SecureRandom.hex
REDIRECT_URI = 'abc://bla'
SCOPE='read_api'

enable :logging

set :host, '0.0.0.0'

get '/' do
  <<~HTML
  OAuth flows
  <ul>
    <li><a href="/implicit">Implicit grant</a></li>
    <li><a href="/password">Resource owner password credentials</a></li>
    <li><a href="/code">Authorization code</a></li>
    <li><a href="/pkce">Authorization code with PKCE</a></li>
  </ul>
  HTML
end

get '/pkce' do
  "<a href=\"#{GITLAB_URL}/oauth/authorize?response_type=code&client_id=#{CLIENT_ID}&redirect_uri=#{REDIRECT_URI}&state=pkce:#{STATE}&code_challenge=#{CODE_CHALLENGE}&code_challenge_method=S256&scope=#{SCOPE}\">Login with GitLab</a>"
end

get '/code' do
  "<a href=\"#{GITLAB_URL}/oauth/authorize?response_type=code&client_id=#{CLIENT_ID}&redirect_uri=#{REDIRECT_URI}&state=code:#{STATE}&scope=#{SCOPE}\">Login with GitLab</a>"
end

# Deprecated in 15.0
get '/implicit' do
  "<a href=\"#{GITLAB_URL}/oauth/authorize?response_type=token&client_id=#{CLIENT_ID}&client_secret=#{CLIENT_SECRET}&redirect_uri=#{REDIRECT_URI}&state=implicit:#{STATE}&scope=profile\">Login with GitLab</a>"
end

get '/password' do
  pp HTTParty.post(
    "#{GITLAB_URL}/oauth/token",
    body: "grant_type=password&username=#{USERNAME}&password=#{PASSWORD}",
    headers: {
      'accept' => 'application/json',
      'cache-control' => 'no-cache'
    }
  )
end

get '/oauth/callback' do
  flow, state = params['state']&.split(':')
  if flow == 'pkce'
    <<~HTML
      <script>
      fetch("#{GITLAB_URL}/oauth/token", {
          method: 'POST',
          headers: {
              "content-type": "application/x-www-form-urlencoded"
          },
          body: "grant_type=authorization_code&client_id=#{CLIENT_ID}&client_secret=#{CLIENT_SECRET}&redirect_uri=#{REDIRECT_URI}&code=#{params['code']}&code_verifier=#{CODE_VERIFIER}"
      })
          .then(res => res.json())
          .then(data => console.log(data));
      </script>
    HTML
  elsif flow.nil?
    # TODO: validate state with JS
    'Check URL fragment for access token'
  else
    return 403, 'invalid state' if state != STATE

    pp HTTParty.post(
      "#{GITLAB_URL}/oauth/token",
      body: "grant_type=authorization_code&client_id=#{CLIENT_ID}&client_secret=#{CLIENT_SECRET}&redirect_uri=#{REDIRECT_URI}&code=#{params['code']}#{flow == 'pkce' ? "&code_verifier=#{CODE_VERIFIER}" : ''}",
      headers: {
        'accept' => 'application/json',
        'cache-control' => 'no-cache'
      }
    )
  end
end
